package com.devcamp_province.provincedistrictward.controller;
 
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp_province.provincedistrictward.model.*;
import com.devcamp_province.provincedistrictward.repository.*;
import com.devcamp_province.provincedistrictward.service.*;

@RequestMapping("/district")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class DistrictController { 
    @Autowired
    IDistrictRepository iDistrictRepository;
    @Autowired
    DistrictService DistrictService;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllDistricts() {
        List<CDistrict> listDistrict = DistrictService.getAllDistricts();
        try {
            return new ResponseEntity<>(listDistrict, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all/provinceId/{provinceId}")
    public ResponseEntity<Object> getDistrictsByProvinceId(@PathVariable int provinceId) {
        List<CDistrict> listDistrict = DistrictService.getDistrictsByProvinceId(provinceId);
        if(listDistrict != null) {
            try {
                return new ResponseEntity<>(listDistrict, HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Province not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable int id) {
        CDistrict existedDistrict = DistrictService.getDistrictById(id);
        if(existedDistrict != null) {
            try {
                return new ResponseEntity<>(existedDistrict, HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createDistrict(@RequestParam int provinceId, @RequestBody CDistrict pDistrict) {
        CDistrict newDistrict = DistrictService.createDistrict(provinceId, pDistrict);
        if(newDistrict != null) {
            try {
                return new ResponseEntity<>(iDistrictRepository.save(newDistrict), HttpStatus.CREATED);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Create District", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Province not found", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateDistrictById(@PathVariable int id, @RequestBody CDistrict pDistrict) {
        CDistrict updatedDistrict = DistrictService.updateDistrictById(id, pDistrict);
        if(updatedDistrict != null) {
            try {
                return new ResponseEntity<>(iDistrictRepository.save(updatedDistrict), HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Update District", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable int id) {
        Optional<CDistrict> existedDistrict = iDistrictRepository.findById(id);
        if(existedDistrict != null) {
            try {
                iDistrictRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Delete District", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
        }
    }
}
