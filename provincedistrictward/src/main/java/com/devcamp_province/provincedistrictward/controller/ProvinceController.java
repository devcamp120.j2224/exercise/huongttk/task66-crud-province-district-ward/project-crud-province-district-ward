package com.devcamp_province.provincedistrictward.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp_province.provincedistrictward.model.*;
import com.devcamp_province.provincedistrictward.repository.*;
import com.devcamp_province.provincedistrictward.service.*;


@RequestMapping("/province")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class ProvinceController {
    @Autowired
    IProvinceRepository iProvinceRepository;
    @Autowired
    ProvinceService ProvinceService;
    
    @GetMapping("/all")
    public ResponseEntity<Object> getAllProvinces() {
        List<CProvince> listProvince = ProvinceService.getAllProvinces();
        try {
            return new ResponseEntity<>(listProvince, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getProvinceById(@PathVariable int id) {
        CProvince existedProvince = ProvinceService.getProvinceById(id);
        if(existedProvince != null) {
            try {
                return new ResponseEntity<>(existedProvince, HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Province not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createProvince(@RequestBody CProvince pProvince) {
        CProvince newProvince = ProvinceService.createProvince(pProvince);
        try {
            return new ResponseEntity<>(iProvinceRepository.save(newProvince), HttpStatus.CREATED);
        } catch(Exception ex) {
            return new ResponseEntity<>("Failed to Create Province", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateProvinceById(@PathVariable int id, @RequestBody CProvince pProvince) {
        CProvince updatedProvince = ProvinceService.updateProvinceById(id, pProvince);
        if(updatedProvince != null) {
            try {
                return new ResponseEntity<>(iProvinceRepository.save(updatedProvince), HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Update Province", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Province not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Object> deleteProvinceById(@PathVariable int id) {
        Optional<CProvince> existedProvince = iProvinceRepository.findById(id);
        if(existedProvince != null) {
            try {
                iProvinceRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Delete Province", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Province not found", HttpStatus.NOT_FOUND);
        }
    }
}
