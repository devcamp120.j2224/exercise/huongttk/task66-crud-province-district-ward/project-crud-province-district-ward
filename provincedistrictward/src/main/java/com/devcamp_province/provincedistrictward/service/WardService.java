package com.devcamp_province.provincedistrictward.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp_province.provincedistrictward.model.*;
import com.devcamp_province.provincedistrictward.repository.*;

@Service
public class WardService {
    @Autowired
    IDistrictRepository iDistrictRepository;
    @Autowired
    IWardRepository iWardRepository;
    
    public List<CWard> getAllWards() {
        List<CWard> listWard = new ArrayList<CWard>();
        iWardRepository.findAll().forEach(listWard::add);
        return listWard;
    }

    public List<CWard> getWardsByDistrictId(int districtId) {
        Optional<CDistrict> existedDistrict = iDistrictRepository.findById(districtId);
        if(existedDistrict.isPresent()) {
            List<CWard> listWard = new ArrayList<CWard>();
            existedDistrict.get().getWards().forEach(listWard::add);
            return listWard;
        } else {
            return null;
        }
    }

    public CWard getWardById(int id) {
        Optional<CWard> existedWard = iWardRepository.findById(id);
        if(existedWard.isPresent()) {
            return existedWard.get();
        } else {
            return null;
        }
    }

    public CWard createWard(int districtId, CWard pWard) {
        Optional<CDistrict> existedDistrict = iDistrictRepository.findById(districtId);
        if(existedDistrict.isPresent()) {
            CWard newWard = new CWard();
            newWard.setPrefix(pWard.getPrefix());
            newWard.setName(pWard.getName());
            newWard.setDistrict(existedDistrict.get());
            return newWard;
        } else {
            return null;
        }
    }

    public CWard updateWardById(int id, CWard pWard) {
        Optional<CWard> existedWard = iWardRepository.findById(id);
        if(existedWard.isPresent()) {
            CWard updatedWard = existedWard.get();
            updatedWard.setPrefix(pWard.getPrefix());
            updatedWard.setName(pWard.getName());
            return updatedWard;
        } else {
            return null;
        }
    }
}
