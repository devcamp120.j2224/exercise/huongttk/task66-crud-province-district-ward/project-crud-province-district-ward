package com.devcamp_province.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp_province.provincedistrictward.model.CWard;

public interface IWardRepository extends JpaRepository<CWard, Integer> {
    
}
