package com.devcamp_province.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp_province.provincedistrictward.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Integer>{
}
