package com.devcamp_province.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp_province.provincedistrictward.model.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Integer>{
}
